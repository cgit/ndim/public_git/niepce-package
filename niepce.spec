Name:		niepce
Version:	0.0.1
Release:	2%{?dist}
Summary:	FIXME

Group:		Applications/Multimedia
License:	GPLv3+ and GPLv2+
# News also on http://www.figuiere.net/hub/blog/?Niepce-digital
URL:		http://live.gnome.org/Niepce
Source0:	%{name}-%{version}.tar.bz2
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	gettext

BuildRequires:	glibmm24-devel >= 2.18.1
BuildRequires:	boost-devel >= 1.33.1
BuildRequires:	gtkmm24-devel >= 2.10.0
BuildRequires:	libglademm24-devel >= 2.6.0
BuildRequires:	GConf2-devel >= 2.0.0
BuildRequires:	sqlite-devel >= 3.0
BuildRequires:	gnome-vfs2-devel >= 2.12
BuildRequires:	exempi-devel >= 2.0.0
BuildRequires:	libxml2-devel >= 2.5.0
BuildRequires:	babl-devel
BuildRequires:	libopenraw-gnome-devel >= 0.0.5
BuildRequires:	gegl-devel >= 0.0.22
BuildRequires:	geglmm-devel >= 0.0.22
BuildRequires:	goocanvasmm-devel >= 0.6.0


%description
FIXME


%prep
%setup -q


%build
%configure
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
%find_lang %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog README
%{_bindir}/niepce
%{_datadir}/%{name}/glade/importdialog.glade
%{_datadir}/%{name}/glade/preferences.glade
%{_datadir}/%{name}/pixmaps/niepce-img-fmt.png
%{_datadir}/%{name}/pixmaps/niepce-jpg-fmt.png
%{_datadir}/%{name}/pixmaps/niepce-png-fmt.png
%{_datadir}/%{name}/pixmaps/niepce-raw-fmt.png
%{_datadir}/%{name}/pixmaps/niepce-rawjpeg-fmt.png
%{_datadir}/%{name}/pixmaps/niepce-rotate-left.png
%{_datadir}/%{name}/pixmaps/niepce-rotate-right.png
%{_datadir}/%{name}/pixmaps/niepce-set-star.png
%{_datadir}/%{name}/pixmaps/niepce-tiff-fmt.png
%{_datadir}/%{name}/pixmaps/niepce-unknown-fmt.png
%{_datadir}/%{name}/pixmaps/niepce-unset-star.png
%{_datadir}/%{name}/pixmaps/niepce-video-fmt.png
%{_datadir}/%{name}/themes/niepce-dark.gtkrc


%changelog
* Fri Feb 13 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.0.1-2
- use mktemp buildroot
- do not ship empty NEWS files

* Thu Feb 12 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.0.1-1
- Initial package

